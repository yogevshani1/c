
#include <iostream>
using namespace std;
const int SIZE = 4;

void printMat(int mat[][SIZE], int rows);
bool isChessBoard(int mat[][SIZE], int rows);

int main()
{
	int mat[SIZE][SIZE];

	cout << " please enter number of 0 and 1 to create chess board :" << endl;

	for (int i = 0; i < SIZE; i++)
	{
		for (int j = 0; j < SIZE; j++)
		{
			cin >> mat[i][j];
		}
	}


	if (isChessBoard(mat, SIZE))
		printMat(mat, SIZE);
	else
	{

		cout << "this chess board is not valid";
	}
}

void printMat(int mat[][SIZE], int rows)
{
	int i, j;

	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < SIZE; j++)
		{
			cout << mat[i][j];
		}

		cout << endl;
	}
}

bool isChessBoard(int mat[][SIZE], int rows)
{
	for (int i = 0; i < SIZE; i++)
	{
		for (int j = 0; j < SIZE; j++)

		{
			if (mat[i][j] == 0 && mat[i][j + 1] == 0 || mat[i][j] == 1 && mat[i][j + 1] == 1)
			{
				return false;

			}
			if (mat[i][0] == 0 && mat[i][SIZE - 1] == 0 || mat[i][0] == 1 && mat[i][SIZE - 1] == 1)
			{

				return false;
			}
			if (mat[i][0] == 0 && mat[i + 1][0]  || mat[i][0] == 1 && mat[i + 1][0] == 1)
			{

				return false;
			}
			if (mat[i][j] != 0 && mat[i][j] != 1)
			{
				return false;
			}
		}


	}
	return true;
}